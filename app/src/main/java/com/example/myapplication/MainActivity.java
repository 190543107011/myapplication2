package com.example.myapplication;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    EditText etfirstname,etlastname;
    TextView tvdisplay;
    Button btnok;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        etfirstname =(EditText) findViewById(R.id.etfirstname);
        etlastname =(EditText) findViewById(R.id.etlastname);
        btnok=(Button)findViewById(R.id.btnok);
        tvdisplay=(TextView)findViewById(R.id.tvdisplay);

        etlastname.getText().toString().trim();
        etlastname.getText().toString().trim();
        btnok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

               String  firstname =etfirstname.getText().toString();
                String  lastname =etlastname.getText().toString();
                String temp =firstname +" "+ lastname;

                tvdisplay.setText(temp);
            }
        });
    }
}
